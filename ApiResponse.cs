﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Lambda
{
    public class ApiResponse
    {
        public object Data;
        public string Error;

        public ApiResponse() { }
        public ApiResponse(string error)
        {
            Error = error;
        }
        public ApiResponse(object data)
        {
            Data = data;
        }
        public ApiResponse(string error, object data)
        {
            Error = error;
            Data = data;
        }
    }
}
