﻿using Amazon.Lambda.APIGatewayEvents;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace MTJ.AWS.Lambda
{
    public class APIFunctionBase
    {
        protected APIGatewayProxyResponse ConstructResponse(HttpStatusCode statusCode)
        {
            return new APIGatewayProxyResponse()
            {
                StatusCode = (int)statusCode,
                Headers = new Dictionary<string, string>
                {
                    { "Access-Control-Allow-Origin", "*" }
                }
            };
        }
        protected APIGatewayProxyResponse ConstructResponse(object body = null)
        {
            APIGatewayProxyResponse response = ConstructResponse(HttpStatusCode.OK);
            if (body != null)
            {
                response.Body = JsonConvert.SerializeObject(body, new JsonSerializerSettings()
                {
                    ContractResolver = new CamelCasePropertyNamesContractResolver()
                });
                response.Headers.Add("Content-Type", "application/json");
            }
            return response;
        }
        protected APIGatewayProxyResponse ConstructOkResponse()
        {
            return ConstructResponse(HttpStatusCode.OK);
        }
    }

    public static class LambdaExtensions
    {
        public static T ParseBody<T>(this APIGatewayProxyRequest request)
        {
            return JsonConvert.DeserializeObject<T>(request.Body);
        }
    }
}
