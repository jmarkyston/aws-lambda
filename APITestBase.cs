﻿using Amazon.Lambda.APIGatewayEvents;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace MTJ.AWS.Lambda
{
    public class APITestBase
    {
        public static APIGatewayProxyRequest ConstructRequest(object body)
        {
            return new APIGatewayProxyRequest()
            {
                Body = JsonConvert.SerializeObject(body)
            };
        }
        public static APIGatewayProxyRequest ConstructRequestWithPath(Dictionary<string, string> parameters, object body = null)
        {
            APIGatewayProxyRequest request = new APIGatewayProxyRequest()
            {
                PathParameters = parameters
            };
            if (body != null)
                request.Body = JsonConvert.SerializeObject(body);
            return request;
        }
        public static APIGatewayProxyRequest ConstructRequestWithQuery(Dictionary<string, string> parameters)
        {
            return new APIGatewayProxyRequest()
            {
                QueryStringParameters = parameters
            };
        }

        public static T ExecuteRequest<T>(APIGatewayProxyRequest request, Func<Task<APIGatewayProxyResponse>> func)
        {
            APIGatewayProxyResponse response = func().Result;
            return JsonConvert.DeserializeObject<T>(response.Body);
        }
    }
}
