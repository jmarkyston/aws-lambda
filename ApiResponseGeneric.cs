﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Lambda
{
    public class ApiResponse<T>
    {
        public T Data;
        public string Error;

        public ApiResponse() { }
        public ApiResponse(string error)
        {
            Error = error;
        }
        public ApiResponse(T data)
        {
            Data = data;
        }
        public ApiResponse(string error, T data)
        {
            Error = error;
            Data = data;
        }
    }
}
