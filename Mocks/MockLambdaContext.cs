﻿using Amazon.Lambda.Core;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTJ.AWS.Lambda.Mocks
{
    public class MockLambdaContext : ILambdaContext
    {
        public string AwsRequestId => throw new NotImplementedException();

        public IClientContext ClientContext => throw new NotImplementedException();

        public string FunctionName => throw new NotImplementedException();

        public string FunctionVersion => throw new NotImplementedException();

        public ICognitoIdentity Identity => throw new NotImplementedException();

        public string InvokedFunctionArn => throw new NotImplementedException();

        private readonly ILambdaLogger _Logger = new MockLambdaLogger();
        public ILambdaLogger Logger => _Logger;

        public string LogGroupName => throw new NotImplementedException();

        public string LogStreamName => throw new NotImplementedException();

        public int MemoryLimitInMB => throw new NotImplementedException();

        public TimeSpan RemainingTime => throw new NotImplementedException();
    }
}
